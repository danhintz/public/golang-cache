package util

import (
	"fmt"
	"testing"
	"time"
)

func Assert(t *testing.T, condition bool) {
	if !condition {
		t.Fail()
	}
}

func TestStringCacheWithoutExpiration(t *testing.T) {
	cache := NewCache[string, string](nil)
	Assert(t, cache != nil)

	v, exists := cache.Get("k1")
	Assert(t, !exists)
	Assert(t, v == nil)

	v1 := "v1"
	cache.Add("k1", &v1)

	v, exists = cache.Get("k1")
	Assert(t, exists)
	Assert(t, v != nil)
	Assert(t, *v == "v1")

	cache.Remove("k1")

	v, exists = cache.Get("k1")
	Assert(t, !exists)
	Assert(t, v == nil)
}

func TestStringCacheWithExpirationWithoutFunction(t *testing.T) {
	expirationSpec := CacheExpirationSpec[string, string]{
		CacheCleanupInterval: 250 * time.Millisecond,
		CacheItemLifespan:    1 * time.Second,
		PreDeleteFunc:        nil,
	}

	cache := NewCache[string, string](&expirationSpec)
	Assert(t, cache != nil)

	v, exists := cache.Get("k1")
	Assert(t, !exists)
	Assert(t, v == nil)

	v1 := "v1"
	cache.Add("k1", &v1)

	v, exists = cache.Get("k1")
	Assert(t, exists)
	Assert(t, v != nil)
	Assert(t, *v == "v1")

	time.Sleep(time.Duration(2 * time.Second))

	v, exists = cache.Get("k1")
	Assert(t, !exists)
	Assert(t, v == nil)
}

func TestStringCacheWithExpirationWithFunction(t *testing.T) {
	// The preDeleteFunction will update this string variable
	var str = ""

	var preDeleteFunction = func(key string, item string) {
		str = fmt.Sprintf("%s:%s", key, item)
	}

	expirationSpec := CacheExpirationSpec[string, string]{
		CacheCleanupInterval: 250 * time.Millisecond,
		CacheItemLifespan:    1 * time.Second,
		PreDeleteFunc:        &preDeleteFunction,
	}

	cache := NewCache[string, string](&expirationSpec)

	v1 := "v1"
	cache.Add("k1", &v1)

	v, exists := cache.Get("k1")
	Assert(t, exists)
	Assert(t, v != nil)
	Assert(t, *v == "v1")

	Assert(t, str == "")

	time.Sleep(time.Duration(2 * time.Second))

	v, exists = cache.Get("k1")
	Assert(t, !exists)
	Assert(t, v == nil)

	Assert(t, str == "k1:v1")
}
