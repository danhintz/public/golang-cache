package util

import (
	"sync"
	"time"
)

type CacheExpirationSpec[K comparable, I any] struct {
	// CacheCleanupInterval is the interval the cache will purge expired items
	CacheCleanupInterval time.Duration

	// CacheItemLifespan is the time a cache item remains in the cache before it is purged
	CacheItemLifespan time.Duration

	// PreDeleteFunc (optional) will be called just before the cache item is purged
	PreDeleteFunc *func(K, I)
}

func NewCache[K comparable, I any](expirationSpec *CacheExpirationSpec[K, I]) CacheInterface[K, I] {
	newCache := &cache[K, I]{
		mutex:          sync.Mutex{},
		data:           make(map[K]cacheItem[I]),
		expirationSpec: expirationSpec,
	}
	if expirationSpec != nil {
		go newCache.purgeExpiredItems()
	}
	return newCache
}

type CacheInterface[K comparable, I any] interface {
	Get(key K) (*I, bool)
	Add(key K, value *I)
	Remove(key K)
}

type cache[K comparable, I any] struct {
	mutex          sync.Mutex
	data           map[K]cacheItem[I]
	expirationSpec *CacheExpirationSpec[K, I]
}

type cacheItem[I any] struct {
	value      *I
	expiration *time.Time
}

func (c *cache[K, I]) Get(key K) (value *I, exists bool) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	item, exists := c.data[key]
	if !exists {
		return nil, false
	}

	if c.expirationSpec != nil {
		if time.Now().After(*(item.expiration)) {
			c.Remove(key)
			return nil, false
		}
	}

	return item.value, true
}

func (c *cache[K, I]) Add(key K, value *I) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	var expiration *time.Time
	if c.expirationSpec != nil {
		exp := time.Now().Add(c.expirationSpec.CacheItemLifespan)
		expiration = &exp
	}

	c.data[key] = cacheItem[I]{
		value:      value,
		expiration: expiration,
	}
}

func (c *cache[K, I]) Remove(key K) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	delete(c.data, key)
}

func (c *cache[K, I]) purgeExpiredItems() {
	for { // infinite loop
		time.Sleep(c.expirationSpec.CacheCleanupInterval)
		c.mutex.Lock()
		for key, item := range c.data {
			if time.Now().After(*(item.expiration)) {
				// If a function was specified, call it before we delete the cache item
				if c.expirationSpec.PreDeleteFunc != nil {
					(*c.expirationSpec.PreDeleteFunc)(key, *item.value)
				}
				// Delete the cache item
				delete(c.data, key)
			}
		}
		c.mutex.Unlock()
	}
}
